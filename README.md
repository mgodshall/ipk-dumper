# ipk-dumper

A small utility for dumping IPK file lists, control files, dumping contents of
a specific file, and searching a bunch of IPKs for a specific file.

## Install

ipk-dumper requires `python-debian`, to install do:

    sudo apt-get install python-debian

Or:

    sudo pip install python-debian


Then:

    cp src/ipk-dumper ~/bin

or wherever you want to install it to.

## Usage

To dump a control file:

    ipk-dumper control <IPK file>

To list file contents:

    ipk-dumper ls <IPK file>

To dump contents of a specific file:

    ipk-dumper dump <path inside IPK to file> <IPK file>

To search for a file in a collection of IPKs:

    ipk-dumper search <full or partial filename> <directory containing IPKs>

